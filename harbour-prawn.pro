# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-prawn

CONFIG += sailfishapp

SOURCES += src/harbour-prawn.cpp

DISTFILES += qml/harbour-prawn.qml \
    qml/Constants.qml \
    qml/cover/CoverPage.qml \
    qml/elements/AnimatedListView.qml \
    qml/elements/Bubble.qml \
    qml/elements/CommentItem.qml \
    qml/elements/LoadingSpinner.qml \
    qml/elements/RoundButton.qml \
    qml/elements/StoryHeader.qml \
    qml/elements/StoryListItem.qml \
    qml/models/JSONListModel.qml \
    qml/models/JSONObjectData.qml \
    qml/models/StoriesModel.qml \
    qml/models/jsonpath.js \
    qml/pages/LinkPage.qml \
    qml/pages/Section.qml \
    qml/pages/Sites.qml \
    qml/pages/StoriesList.qml \
    qml/pages/Story.qml \
    qml/utils.js \
    qml/timeago.js \
    rpm/harbour-prawn.changes.in \
    rpm/harbour-prawn.changes.run.in \
    rpm/harbour-prawn.spec \
    rpm/harbour-prawn.yaml \
    translations/*.ts \
    harbour-prawn.desktop \
    qml/models/StoryModel.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-prawn-it.ts
