# Prawn

Read-only SailfishOS client for [Lobste.rs](https://github.com/lobsters/lobsters) and [sister sites](https://github.com/lobsters/lobsters/wiki#sister-sites) 

- choose which site to read, or enter your personal site url
- see posts ordered by hottest and newest
- read threaded comments
- open links in browser or copy to clipboard
- filter by tags