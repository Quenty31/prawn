/*
    Copyright (C) 2014  Dickson Leong
    Copyright (C) 2015-2017  Sander van Grieken
    Copyright (C) 2019 Fabio Comuni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see [http://www.gnu.org/licenses/].
*/
import QtQuick 2.0
import Sailfish.Silica 1.0

QtObject {
    id: constants


    readonly property color colorLight: Theme.primaryColor
    readonly property color colorMid: Theme.secondaryColor
    readonly property color colorDisabled: Theme.colorScheme ? Qt.lighter(colorMid, 1.5) : Qt.darker(colorMid, 1.5)
    readonly property color colorMidLight: Theme.colorScheme ? Qt.lighter(colorLight, 1.2) : Qt.darker(colorLight, 1.2)
    readonly property color colorHi: Theme.colorScheme ? Qt.darker(Theme.highlightColor, 1.1) : Qt.lighter(Theme.highlightColor, 1.1)

    readonly property color colorLikes: "#FF8B60"
    readonly property color colorDislikes: "#9494FF"

    readonly property var indentColors: [
        "#5DA5DA",
        "#FAA43A",
        "#60BD68",
        "#F17CB0",
        "#B2912F",
        "#B276B2",
        "#DECF3F",
        "#F15854",
    ]


    readonly property string richtextStyle: contentStyle(true)

    function contentStyle(enabled) {
        return "<style>del {text-decoration: line-through;} h1 { font-size: x-large; } " +
               "table { font-family: monospace; font-size: small; } thead { text-decoration: underline ; } td { padding-right: 20px; } " +
               "a { color: " + (enabled ? Theme.highlightColor : colorDisabled) + "; } " +
               "code { color: " + (enabled ? colorHi : colorDisabled) + "; } </style>"
    }
}
