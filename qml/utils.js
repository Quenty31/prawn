.pragma library

function s2str(secs) {
    secs = Math.round(secs);
    var r = (secs%60);
    if (r<10) r = "0" + r;

    var mins = Math.floor(secs/60);
    if (mins>0) {
        r = (mins%60)+":"+r
        if ((mins%60)<10) r = "0" + r;
    } else {
        r = "00:" + r
    }

    var hours = Math.floor(mins/60);
    if (hours>0)
        r = (hours%60)+":"+r

    return r;
}

function json2date(str) {
    return new Date(Date.parse(str));
}


var getHostRe = new RegExp(".*://([^/]*)(/.*|$)");
function getHost(url) {
    return url.replace(getHostRe, "$1");
}

var getBaseUrlRe = new RegExp("(https?://[^/]*)(/.*|$)");
function getBaseUrl(url) {
    return url.replace(getBaseUrlRe, "$1");
}


function isInternalStoryLink(site, url) {
    var reInternalStoryRe = new RegExp("^" + site + "/s/");
    if (url === undefined || url.match === undefined) return false;
    return url.match(reInternalStoryRe) !== null;
}
