import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property string link
    property string url

    Column {
        width: parent.width

        spacing: Theme.paddingMedium

        PageHeader {
            title: qsTr("Open URL")
        }


        Label {
            visible: url !== ""
            text: url
            color: constants.colorLight
            wrapMode: Text.WordWrap
            x: Theme.paddingMedium
            width: parent.width - (x * 2)
        }

        Row {
            visible: url !== ""
            x: Theme.paddingMedium
            width: parent.width - (x * 2)

            spacing: Theme.paddingMedium

            Button {
                width: parent.width  / 2 - parent.spacing
                text: qsTr("Open in browser")
                onClicked: doManageLink(link, false)
            }

            Button {
                width: parent.width  / 2 - parent.spacing
                text: qsTr("Copy URL")
                onClicked: Clipboard.text = link
            }
        }

        Rectangle {
            visible: url !== ""
            color: "#00000000"
            height: Theme.paddingLarge * 2
            width: parent.width
            x: 0
        }


        Label {
            visible: link !== ""
            text: link
            color: constants.colorLight
            wrapMode: Text.WordWrap
            x: Theme.paddingMedium
            width: parent.width - (x * 2)
        }

        Row {
            visible: link !== ""
            x: Theme.paddingMedium
            width: parent.width - (x * 2)

            spacing: Theme.paddingMedium

            Button {
                width: parent.width  / 2 - parent.spacing
                text: qsTr("Open in browser")
                onClicked: doManageLink(link, false)
            }

            Button {
                width:parent.width / 2 - parent.spacing
                text: qsTr("Copy link")
                onClicked: Clipboard.text = link
            }
        }
    }


}
