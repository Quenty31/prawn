import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../elements"

import "../utils.js" as Utils

Page {
    id: page

    property string section: "/hottest"

    allowedOrientations: Orientation.All

    function onThisPage() {
        // something something cover
    }

    Connections {
        target: conf
        onSiteChanged: pagemodel.model.clear()
    }

    StoriesModel {
        id: pagemodel
        source: conf.site + section + ".json"
    }


    SilicaListView  {
        id: listView

        PullDownMenu {
            MenuItem {
                text: qsTr("Site")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("Sites.qml"),
                                                {"site": ""})
                    dialog.accepted.connect(function() {
                        conf.site = dialog.site
                    })
                }
            }

            MenuItem {
                text: qsTr("Section")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("Section.qml"),
                                                {"section": page.section})
                    dialog.accepted.connect(function() {
                        page.section = dialog.section
                    })
                }
            }

            MenuItem {
                text: qsTr("Refresh")
                onClicked: pagemodel.reload()
            }
            /*MenuItem {
                text: qsTr("Trending")
                onClicked: {
                    listorder = "trending"
                    videoListModel.orderTrending();
                    title = qsTr("Trending")
                }
               font.bold: listorder === "trending"
            }
            MenuItem {
                text: qsTr("Recent")
                onClicked: {
                    listorder = "recent"
                    videoListModel.orderRecent();
                    title = qsTr("Recent")
                }
               font.bold: listorder === "recent"
            }
            MenuItem {
                text: listfilter === "local" ? qsTr("Show all") : qsTr("Show local")
                onClicked: {
                    if (listfilter === "local") {
                        listfilter =  "";
                        description = "";
                        videoListModel.filterAll();
                    } else {
                        listfilter = "local";
                        description = qsTr("local");
                        videoListModel.filterLocal();
                    }
                }
            }

            MenuItem {
                text: qsTr("Search")
                onClicked:pageStack.push(Qt.resolvedUrl("SearchPage.qml"));
            }*/
        }

        model: pagemodel.model
        anchors.fill: parent

        onAtYEndChanged: {
            if (atYEnd && pagemodel.status > 0) pagemodel.loadMore();
        }

        header: PageHeader {
            id: pageHeader
            title: section
            description: Utils.getHost(conf.site+"/")
        }


        delegate: StoryListItem {
            onClicked: pageStack.push(Qt.resolvedUrl("Story.qml"), {story:model})
        }

        ViewPlaceholder {
            enabled: pagemodel.count == 0 && pagemodel.status > 0
            text: qsTr("No items")
            // hintText: qsTr("Try to select another instance")
        }

        VerticalScrollDecorator {}
    }

    LoadingSpinner {
        model: pagemodel
    }
}
