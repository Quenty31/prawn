import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../elements"

import "../utils.js" as Utils

Page {
    id: page

    property variant story

    StoryModel {
        id: storymodel
        source: story.comments_url + ".json"

        onStoryReady: {
            story = obj;
            comments.model = story.comments;
        }
    }


    SilicaListView {
        id: comments
        anchors.fill: parent

        model: null
        header: headerComponent

        PullDownMenu {
           MenuItem {
               text: qsTr("Refresh")
               onClicked: storymodel.reload()
           }
        }


        Component {
            id: headerComponent


            Column {
                id: column
                width: parent.width
                height: childrenRect.height

                spacing: Theme.paddingMedium

                PageHeader {
                    id: pageHeader
                    title: qsTr("Comments")
                    //description: page.description
                }


                StoryHeader {
                    id: storyheader
                    story: page.story
                    height: childrenRect.height

                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: Theme.paddingMedium
                    }

                    onTagClicked: function(label) {
                        console.log("tag", label)
                        pageStack.push(Qt.resolvedUrl("StoriesList.qml"),
                                        {"section": "/t/" + label})
                    }
                }

                Row {
                    anchors { left: parent.left; right: parent.right; margins: Theme.paddingMedium }
                    spacing: Theme.paddingMedium


                    IconButton {
                        enabled: Utils.isInternalStoryLink(conf.site, story.url)
                        icon.source: "image://theme/icon-m-forward"
                        onClicked: doManageLink(story.url, true)
                    }


                    IconButton {
                        icon.source: "image://theme/icon-m-link"
                        onClicked:  pageStack.push(Qt.resolvedUrl("LinkPage.qml"), { 'link': story.comments_url, 'url': story.url})
                    }

                }


                Separator {
                    anchors { left: parent.left; right: parent.right }
                    color: constants.colorMid
                }

                Label {
                    id: descriptiontext
                    visible: story.description !== ""
                    anchors { left: parent.left; right: parent.right; margins: Theme.paddingMedium }
                    textFormat: Text.RichText
                    wrapMode: Text.WordWrap
                    text:  constants.richtextStyle + story.description
                }

                Separator {
                    visible: descriptiontext.visible
                    anchors { left: parent.left; right: parent.right }
                    color: constants.colorMid
                }



            }
        }


        delegate: CommentItem {}

        ViewPlaceholder {
            enabled: storymodel.status > 0 && storymodel.obj.comments.length === 0
            text: qsTr("No comments")
            verticalOffset: comments.headerItem.height  -  (page.height / 2) + (Theme.fontSizeHuge)
            // hintText: qsTr("Try to select another instance")
        }


        VerticalScrollDecorator {}
    }

    LoadingSpinner {
        model: storymodel
    }
}
