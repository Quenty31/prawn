import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../elements"


Dialog {
    id: dialog
    property string site

    SilicaListView {
        id: comments
        anchors.fill: parent

        model: ListModel {
            ListElement { url:'https://www.barnacl.es'; title:'Barnacles'; desc:'Business' }
            ListElement { url:'https://www.journalduhacker.net/'; title:'Le Journal du Hacker'; desc:'Computer Science (fr)' }
            ListElement { url:'https://cryptobuzz.io/'; title:'CryptoBuzz'; desc:'Cryptocurrency' }
            ListElement { url:'http://news.kindandgreenworld.com/'; title:'Kind and Green News'; desc:'Biology' }
            ListElement { url:'https://www.ghtechroundup.com/'; title:'Gh Tech Roundup'; desc:'Technology' }
            ListElement { url:'https://g33kz.de/'; title:'G33kz'; desc:'Technology (de)' }
            ListElement { url:'https://tilde.news/'; title:'Tildeverse news'; desc:'Computer Science' }
            ListElement { url:'https://std.bz/'; title:'Standard'; desc:'Cryptocurrency' }
            ListElement { url:'https://bitmia.com/'; title:'Bitmia'; desc:'Finance' }
            ListElement { url:'https://gambe.ro'; title:'Gambe.ro'; desc:'Software development (it)' }
            ListElement { url:'https://middlebit.com/'; title:'Middlebit'; desc:'Content curation for the "no-coast" region of the United States.' }
            ListElement { url:'http://science.solobsd.org/'; title:'Quantum News'; desc:'Science' }
            ListElement { url:'https://concat.id'; title:'Concat'; desc:'Computing (Indonesian)' }
            ListElement { url:'https://acrab.isi.jhu.edu/'; title:'ACRAB'; desc:'Applied Cryptography Publications' }
        }

        header: headercomponent
        Component {
            id: headercomponent
            Column {
                id: column
                width: parent.width
                height: childrenRect.height
                spacing: Theme.paddingMedium
                PageHeader {
                    id: pageHeader
                    title: qsTr("Change site")
                }
                TextField {
                    id: customUrl
                    anchors { left: parent.left; right: parent.right }
                    placeholderText: qsTr("Custom site url...")
                    inputMethodHints: Qt.ImhNoPredictiveText
                }
            }
        }


        delegate: ListItem {
            contentHeight: column.height + (2* Theme.paddingMedium)
            Column {
                anchors {
                    left: parent.left;
                    right: parent.right;
                    top: parent.top
                    margins: Theme.paddingMedium
                }
                id: column
                height: childrenRect.height

                Label {
                    anchors { left: parent.left; right: parent.right; }
                    color: constants.colorLight
                    font.bold: true
                    text: title
                }
                Label {
                    anchors { left: parent.left; right: parent.right; }
                    color: constants.colorMid
                    text: url
                }
                Label {
                    anchors { left: parent.left; right: parent.right; }
                    color: constants.colorLight
                    text: desc
                }
            }
            onClicked: {
                dialog.site = url
                dialog.accept()
            }
        }

        VerticalScrollDecorator {}
    }


    onDone: {
        if (result == DialogResult.Accepted) {
            if (customUrl.text !== "") {
                dialog.site = customUrl.text
            }
        }
    }

}
