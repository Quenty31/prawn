/*
    Copyright (C) 2018-2019 Fabio Comuni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see [http://www.gnu.org/licenses/].
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

import "../utils.js" as Utils
import "../timeago.js"as TimeAgo


Column {
    property variant story
    property bool compact: false
    property bool highlighted: false

    signal tagClicked(string label)

    spacing: Theme.paddingSmall

    Flow {
        anchors { left: parent.left; right: parent.right }
        spacing: Theme.paddingMedium

        Repeater {
            model: story.tags
            delegate: Bubble {
                text: model.label
                MouseArea {
                    enabled: !compact
                    anchors.fill: parent
                    onClicked: tagClicked(model.label)
                }
            }
        }
    }


    Label {
        id: title
        text: story.title + ( story.url !== "" ? " (%1)".arg(Utils.getHost(story.url)) : "" )
        color: highlighted ? constants.colorHi : constants.colorLight
        elide: compact ? Text.ElideNone : Text.ElideRight
        wrapMode: Text.WordWrap
        maximumLineCount: compact ? 3 : 999
        font { bold: true }
        anchors { left: parent.left; right: parent.right }
    }
    Label {
        id: submited
        textFormat: Text.RichText
        text:  constants.richtextStyle +
               qsTr("sent %1 by %2").arg(TimeAgo.format(story.created_at)).arg(story.submitter_user.username)
        color: highlighted ? constants.colorHi : constants.colorMid
        wrapMode: Text.WordWrap
        elide: compact ? Text.ElideRight : Text.ElideNone
        maximumLineCount: compact ? 2 : 9999 /* TODO : maxint */
        anchors { left: parent.left; right: parent.right }
        onLinkActivated: doManageLink(link)
    }
    Label {
        id: meta
        text:  qsTr("%1 points - %2 comments").arg(story.score).arg(story.comment_count)
        color: highlighted ? constants.colorHi : constants.colorLight
        wrapMode: Text.WordWrap
        anchors { left: parent.left; right: parent.right }
    }

}
