import QtQuick 2.0
import Sailfish.Silica 1.0

import "../utils.js" as Utils
import "../timeago.js"as TimeAgo

ListItem {
    id: delegate
    property bool compact: true
    readonly property int padding: Theme.paddingMedium


    contentHeight: messageColumn.height + (padding * 2)
    anchors {
        left: parent.left
        right: parent.right
    }

    StoryHeader {
        id: messageColumn

        story: model
        compact: true
        highlighted: delegate.highlighted

        height: childrenRect.height

        anchors {
            left: parent.left
            right: parent.right
            margins: Theme.paddingMedium
            verticalCenter: parent.verticalCenter
        }



    }

}
