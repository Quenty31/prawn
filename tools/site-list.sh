#!/bin/bash

WIKI_URL="https://github.com/lobsters/lobsters.wiki.git"
WIKI_DIR="lobsters.wiki"

cd /tmp
git clone "$WIKI_URL"
cd "$WIKI_DIR"

SITESWD=`cat Home.md | sed -e '/## Cousin/,$d' | sed -n -e '/## Sister/,$p' | grep "^* " | sed "s/* \[\([^]]*\)\](\([^)]*\)).* - \(.*\)/\1;\2;\3/" | grep -v "^* "`
SITESND=`cat Home.md | sed -e '/## Cousin/,$d' | sed -n -e '/## Sister/,$p' | grep "^* " | sed "s/* \[\([^]]*\)\](\([^)]*\))[^-]*/\1;\2;/" | grep -v '\- '`
SITES=`echo -e "$SITESWD\n$SITESND" `

while IFS=';' read title url desc
do
    
    if `curl --max-time 2 $url > /dev/null 2>/dev/null`
    then
        echo "ListElement { url:'$url'; title:'$title'; desc:'$desc' }"
    fi
done <<<"$SITES"

cd ..
rm -fr "$WIKI_DIR"

