<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>CommentItem</name>
    <message>
        <source>%1 points</source>
        <translation>%1 punti</translation>
    </message>
</context>
<context>
    <name>LinkPage</name>
    <message>
        <source>Open URL</source>
        <translation>Apri URL</translation>
    </message>
    <message>
        <source>Open in browser</source>
        <translation>Apri nel browser</translation>
    </message>
    <message>
        <source>Copy URL</source>
        <translation>Copia URL</translation>
    </message>
    <message>
        <source>Copy link</source>
        <translation>Copia link</translation>
    </message>
</context>
<context>
    <name>Section</name>
    <message>
        <source>Filter by tag</source>
        <translation>Filtra per tag</translation>
    </message>
    <message>
        <source>Hottest</source>
        <translation>Caldi</translation>
    </message>
    <message>
        <source>Newest</source>
        <translation>Nuovi</translation>
    </message>
</context>
<context>
    <name>Sites</name>
    <message>
        <source>Change site</source>
        <translation>Cambia sito</translation>
    </message>
    <message>
        <source>Custom site url...</source>
        <translation>URL sito personalizzato...</translation>
    </message>
</context>
<context>
    <name>StoriesList</name>
    <message>
        <source>No items</source>
        <translation>Nessun elemento</translation>
    </message>
    <message>
        <source>Site</source>
        <translation>Sito</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>Sezione</translation>
    </message>
</context>
<context>
    <name>Story</name>
    <message>
        <source>Comments</source>
        <translation>Commenti</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <source>No comments</source>
        <translation>Nessun commento</translation>
    </message>
</context>
<context>
    <name>StoryHeader</name>
    <message>
        <source>sent %1 by %2</source>
        <translation>inviato %1 da %2</translation>
    </message>
    <message>
        <source>%1 points - %2 comments</source>
        <translation>%1 punti - %2 commenti</translation>
    </message>
</context>
<context>
    <name>timeago</name>
    <message>
        <source>second</source>
        <translation>secondo</translation>
    </message>
    <message>
        <source>minute</source>
        <translation>minuto</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>ora</translation>
    </message>
    <message>
        <source>day</source>
        <translation>giorno</translation>
    </message>
    <message>
        <source>week</source>
        <translation>settimana</translation>
    </message>
    <message>
        <source>month</source>
        <translation>mese</translation>
    </message>
    <message>
        <source>year</source>
        <translation>anno</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation>secondi</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation>minuti</translation>
    </message>
    <message>
        <source>hours</source>
        <translation>ore</translation>
    </message>
    <message>
        <source>days</source>
        <translation>giorni</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>settimane</translation>
    </message>
    <message>
        <source>months</source>
        <translation>mesi</translation>
    </message>
    <message>
        <source>years</source>
        <translation>anni</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>adesso</translation>
    </message>
    <message>
        <source>right now</source>
        <translation>adesso</translation>
    </message>
    <message>
        <source>%1 %2 ago</source>
        <extracomment>%number %unit ago (eg. 2 days ago)</extracomment>
        <translation>%1 %2 fa</translation>
    </message>
    <message>
        <source>in %1 %2</source>
        <extracomment>in %number %unit (eg. in 2 days)</extracomment>
        <translation>tra %1 %2</translation>
    </message>
</context>
</TS>
